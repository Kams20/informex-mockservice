const mbHelper = require('./mountebank-helper');
const settings = require('./setting');
const StaticData = require('../src/staticData')

function addService() {
    const response = { headers:{status: StaticData.StaticHeaders},body: StaticData.StaticResponse }

    const stubs = [
        {
            predicates: [ {
                equals: {
                    method: "GET",
                    "path": "/v4/e185cffdc49e36745d483d96ba68c860522a995c2c2e32508bed95cedeb96ba0/D/vin/WAUZZZ8K8DA216098/BEL-nl"
                }
            }],
            responses: [
                {
                    is: {
                        statusCode: 200,
                        headers: {
                            "Content-Type": "application/json"
                        },
                        body: JSON.stringify(response)
                    }
                }
            ]
        }
    ];

    const imposter = {
        port: settings.mockService_port,
        protocol: 'http',
        stubs: stubs
    };

    return mbHelper.postImposter(imposter);
}

module.exports = { addService };