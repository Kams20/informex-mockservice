const mb = require('mountebank');
const settings = require('./setting');
const mockService = require('./mockService');

const mbServerInstance = mb.create({
        port: settings.port,
        pidfile: '../mb.pid',
        logfile: '../mb.log',
        protofile: '../protofile.json',
        ipWhitelist: ['*']
    });

    mbServerInstance.then(function() {
        mockService.addService();
    });
    