const StaticResponse = {
    "registration": {
    "plate": "1EUW960",
    "vin": "WAUZZZ8K8DA216098",
    "registrationFirst": "2013-03-26",
    "registrationLast": "2013-03-26",
    "registrationStatus": "UR",
    "category": {
    "be": "AC",
    "eu": "M1"
    }
    },
    "technical": {
    "name": "Audi A4 (2011) (B8) Attraction",
    "color": "GREY",
    "kw": 100,
    "cc": 1968,
    "co2": 129,
    "massInRunning": 1650,
    "massMaxPermissible": 2145,
    "seats": 5,
    "height": 1415,
    "length": 4699,
    "width": 1826,
    "sportivityRatio": 0.0674,
    "body": {
    "coupe": "N",
    "cabriolet": "N",
    "suv": "N"
    },
    "labels": {
    "manufacturer": "Audi",
    "model": "A4 (2011) (B8)",
    "type": "Attraction"
    },
    "segment": "D",
    "motorType": "DIE"
    },
    "fraudDetection": {
    "previousClaimInLast60Days": false,
    "previousClaimInLast12Months": false,
    "mileageInconsistency": true,
    "totalLossRecorded": false
    },
    "damageCosts": {
    "windshieldReplacement": 740.87,
    "averageRepairCost": 1502.02,
    "reparabilityRatio": 0.964,
    "averageResidualValue": 7989.7367,
    "averageResidualValueRatio": 0.5182
    },
    "sinistrality": {
    "sinistralityRatio": 0.1043,
    "riskAgainstSegment": "NORMAL"
    },
    "carValue": {
    "available": true,
    "minimumCatalogPrice": 28685.95
    },
    "safety": {
    "equipments": {
    "LIGHTS": {
    "LIGHTS_AUTO": "Y",
    "LIGHTS_EMERGENCY": "Y"
    },
    "BREAKING": {
    "BREAKING_ABS": "Y",
    "BREAKING_BA": "Y",
    "BREAKING_EBD": "Y",
    "BREAKING_ASR_TCS": "Y",
    "BREAKING_TMPS_RDC": "Y"
    },
    "PEOPLE_PROTECTION": {
    "PEOPLE_PROTECTION_AIRBAG": "Y",
    "PEOPLE_PROTECTION_SIDEBAG_AV": "Y",
    "PEOPLE_PROTECTION_WINDOWBAG": "Y"
    },
    "PARKING": {
    "PARKING_REAR_PARK_SENSOR": "Y"
    },
    "PROTECTION": {
    "PROTECTION_ALARM_SYSTEM": "Y",
    "PROTECTION_IMMOBILIZER": "Y"
    }
    }
    },
    "mappings": {
    "audatex": {
    "manufacturer": {
    "code": "00",
    "name": "Audi"
    },
    "model": {
    "code": "73",
    "name": "A4 (2011) (B8)"
    }
    },
    "technicar": {
    "ids": [
    122305,
    120048,
    137686,
    137684
    ]
    }
    }
    };

    const StaticHeaders =  [
        {
        "code": {
        "name": "OK",
        "type": "INFO"
        }
        }
        ];
    

    module.exports = { StaticResponse,StaticHeaders };